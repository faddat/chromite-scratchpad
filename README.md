# Virgo Hardware
The Virgo Hardware project is building a simple, secure open source computer.  
Along the way, we learned that it should be possible to fabricate a Shakti C 
class or Chromite core CPU for $20,000-$30,000 and we decided to go for it.  

So our Single-Board Computer project gained a software and infrastructure
component when this happened.  


## Families of RISC-V Processors

Note: Families in this case refer to the build process

* Bluespec
* Chisel



## Build Pipeline
We have learned a ton.  One of the things that we've learned is that the tooling for various linux capable risc-v cores is kind of a mishmash of patches.  

In order to make working on these cores more approachable, we:

* Created a build cluster consisting of 
    * Two 128GB RAM, 4TB RAID 0 NVME SSD, 12 Core AMD Ryzen CPU machines
    * One 64GB RAM, 2TB RAID 0 NVME SSD, 8 Core AMD Ryzen CPU Machines

.....this part was basically about making things go fast.

The cluster doesn't actually need to be so large at this time, but we had some
spare horsepower and just put the runner on all of them.  


* Created a three step build pipeline:

`sim`, `fpga`, and `asic` have not yet been created.  Probably there is even more to go.  

Using these tools, in this order, from our GitLab group, you can modify, test, and  simulate the Shakti C class in about an hour with no need whatsoever to configure a machine.  The entire process runs against our buildboxes and eventually an AWS F1 FPGA.  

We're going to continue to document this carefully, and try to make each step as obvious as possible, treating each piece as a function that passes outputs to the next piece.  



## Shakti Class C

This one builds, it's the one specified in master branch version of faster.sh, builds, runs tests and builds a linux for it.

## Using this repository to test new/changed processor designs

This repository runs the script in faster.sh inside a development environment with the full toolchain to build 
a CPU simulator.

To test new designs, simply add the build script (in place) in your own branch and the runner will attempt to build your
specification.

## Viable Cores For an SBC ASIC with basic SOC
There are far more viable cores than I once thought!  One of the goals of this repo is to ensure that 


Here they are, with some notes/history on each:

#### [Shakti C Class](https://gitlab.com/shaktiproject/cores/c-class)



#### [InCore Semiconductor Chromite](https://gitlab.com/incoresemi/core-generators/chromite)



#### [Ariane](https://github.com/pulp-platform/ariane)
Used in [Core-V Chassis](https://www.openhwgroup.org/news/2019/12/10/openhw-group-announces-core-v-chassis-soc-project-and-issues-industry-call-for-participation/
)

#### [Bluespec Toooba](https://github.com/bluespec/Toooba)



#### [Bluespec Flute](https://github.com/bluespec/Flute)


#### [rvSOC](https://github.com/WangXuan95/USTC-RVSoC)


## Links
https://github.com/riscv/riscv-cores-list

## SOCs

#### [https://github.com/sergeykhbr/riscv_vhdl]





## Design Tools

##### [Ripes](https://github.com/mortbopet/Ripes)
GUI Risc-v design tool and simulator

#### [Chipyard](https://github.com/ucb-bar/chipyard)
Berkeley project enabling users to quickly deisgn/test SoCs

## Dev Platforms

#### [FireSim](https://github.com/firesim/firesim)


## Linux Distributions

#### Debian

#### [Ataraxia Linux](https://github.com/ataraxialinux/ataraxia)


## Interesting

#### [Rust on RISC-V](https://github.com/rust-embedded/riscv)


