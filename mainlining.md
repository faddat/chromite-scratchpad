# Mainlining Shakti C Class and Chromite

I think that what's needed is to create a configuration for mainline linux that will run on Shakti C class/ Chromite, and then probably get that configuration mainlined, too.
The build can be sped up a great deal with various techniques, however it occurred to me that because of the large number of upstream repos that we've got to consume to produce artifacts, it may make the most sense to run the full build process frequently; this will keep us abreast of upstream changes
Thinking like... what if we lived in a world where the stack was built 100x a day, with builds triggered from devs all over?  Not enough devs now of course.... How to get them interested?

Make it easy and far less time consuming!
You can see what I'd like to see for Shakti C/Chromite here:

https://notsyncing.net/?p=blog&b=2016.orangepi-pc-custom-kernel
Building a Custom Mainline Linux Kernel System on the Orange Pi PC ...
This post will explain how to build an Arch Linux system with the latest mainline Linux kernel for the Orange Pi PC by Xunlong from scratch.
the sunxi_defconfig part.  I worked a lot with that board and life got way, way easier once it was fully supported in mainline linux.

But I think that for C Class and Chromite, we may also have to dream up exactly what that configuration is.

I think some of that config lives in the shakti-linux repo now. 
