# Cosmos Core 

The Cosmos Core is an accelerated development, manufacturing and commercialization pipeline for Open Source CPU cores.  

## Components

* GitLab CI
* Build Cluster for GitLab Runners
* Dockerized Development Environments
* RISC-V CPU Cores written in Bluespec
* 

## Goals
A 


## Culture


## Blazing Fast Automated builds for the community

Currently a developer needs to do a whole lot of individual steps to work on a core or software for a core.

I'd like to provide a build server so that a developer can build/test rapidly, like people currently do with software.  I have the tiniest bit of it built so far.
For example, I'll be taking my current configuration, putting it in another repository, and seeing if I can automate getting the freertos running on the core.  Then, devs can work from a "known good state".
Our main bottleneck right now is build times; but from what I can tell we can reduce build times by adding CPU cores, so that's how I chose that particular CPU.

There are other options, like reducing the number of things that we actually build, but I think in the end there's still going to be a whole lot of CPU intensive compiling happening; for example with Linux.

It would be nice to start doing lots of automated tests.
@Madhu Any idea how to build Linux for Shakti C class BTW? Seems shakti linux is no longer needed because riscv-linux has been mainlined.
I personally run some very fast servers, but they just weren't fast enough for this task.  Still a 2-3 hour build time.

I think that what's needed is to create a configuration for mainline linux that will run on Shakti C class/ Chromite, and then probably get that configuration mainlined, too.
The build can be sped up a great deal with various techniques, however it occurred to me that because of the large number of upstream repos that we've got to consume to produce artifacts, it may make the most sense to run the full build process frequently; this will keep us abreast of upstream changes
Thinking like... what if we lived in a world where the stack was built 100x a day, with builds triggered from devs all over?

Good CPU:
* https://amd.com/en/products/cpu/amd-ryzen-threadripper-3990x

## Accomplished so far

* Learned the build process
* Upgraded the 


## Finances
As we are a fully-open project, here's the financial rundown so far:

## Next Steps







